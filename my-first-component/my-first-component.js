class MyFirstComponent extends Polymer.Element {

  static get is() {
    return 'my-first-component';
  }

  static get properties() {
    return {
      title: {
        type: String,
        value: '',
        notify:true
      },
      alternateTitle: {
        type: String,
        value: ''
      }
    };
  }

  _handleAlternateTitleClick() {
    //cambiamos el vlaor de nuestra propiedad. con this hacemos referencia a nosotros
    this.alternateTitle= 'Titulo cambiado por manejador'
  }

  _handleMessageSend() {
    this.alternateTitle= 'Titulo cambiado por _handleMessageSend'
  }
}

customElements.define(MyFirstComponent.is, MyFirstComponent);
